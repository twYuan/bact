package com.yuanlin.bact.data.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yuanlin.bact.data.domain.Activs;

import java.util.HashMap;
import java.util.Map;

public class ActivDetailRepository extends FirebaseDatabaseRepository<Activs> {
    private static final String rootNode = "Activity";
    private String type;
    private String id;

    public ActivDetailRepository(String type, String id) {
        super(Activs.class);
        this.type = type;
        this.id = id;
        this.databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode());
    }

    @Override
    protected String getRootNode() {
        return rootNode + "/" + type + "/" + id;
    }

    public void writeAttend(Map<String,Boolean> attend) {
        if (attend.size() == 0) {
            ((DatabaseReference) databaseReference).child("attend").removeValue();
        } else {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.putAll(attend);
            ((DatabaseReference) databaseReference).child("attend").updateChildren(childUpdates);
        }
    }

    public void writeFavorite(Map<String,Boolean> attend) {
        if (attend.size() == 0) {
            ((DatabaseReference) databaseReference).child("favorite").removeValue();
        } else {
            Map<String, Object> childUpdates = new HashMap<>();
            childUpdates.putAll(attend);
            ((DatabaseReference) databaseReference).child("favorite").updateChildren(childUpdates);
        }
    }
}
