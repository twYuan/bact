package com.yuanlin.bact.data.domain;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@IgnoreExtraProperties
public class Activs {

    public String createDate;
    public String creatorId;
    public String creatorName;
    public String description;
    public String location;
    public String endDate;
    public List<String> img = null;
    public List<String> member = null;
    public String name;
    public String maxMember;
    public String startDate;
    public String activId;
    public Map<String,Boolean> attend = new HashMap<>();
    public Map<String,Boolean> favorite = new HashMap<>();

    public Activs() {
    }

    public Activs(String createDate, String creatorId, String creatorName, String description, String location, String endDate, List<String> img, List<String> member, String name, String startDate) {
        this.createDate = createDate;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
        this.description = description;
        this.location = location;
        this.endDate = endDate;
        this.img = img;
        this.member = member;
        this.name = name;
        this.startDate = startDate;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("createDate", createDate);
        result.put("creatorId", creatorId);
        result.put("creatorName", creatorName);
        result.put("description", description);
        result.put("location", location);
        result.put("endDate", endDate);
        result.put("img", img);
        result.put("member", member);
        result.put("name", name);
        result.put("startDate", startDate);
        result.put("activId", activId);
        result.put("attend", attend);
        result.put("favorite", favorite);
        return result;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }

    public List<String> getMember() {
        return member;
    }

    public void setMember(List<String> member) {
        this.member = member;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaxMember() {
        return maxMember;
    }

    public void setMaxMember(String maxMember) {
        this.maxMember = maxMember;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getActivId() {
        return activId;
    }

    public void setActivId(String activId) {
        this.activId = activId;
    }

    public Map<String, Boolean> getAttend() {
        return attend;
    }

    public void setAttend(Map<String, Boolean> attend) {
        this.attend = attend;
    }

    public Map<String, Boolean> getFavorite() {
        return favorite;
    }

    public void setFavorite(Map<String, Boolean> favorite) {
        this.favorite = favorite;
    }
}
