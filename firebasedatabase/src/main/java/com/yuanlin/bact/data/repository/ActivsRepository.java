package com.yuanlin.bact.data.repository;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yuanlin.bact.data.domain.Activs;
import com.yuanlin.bact.data.mapper.ActivsMapper;

import java.util.HashMap;
import java.util.Map;

public class ActivsRepository extends FirebaseDatabaseRepository<Activs> {
    private static final String rootNode = "Activity";
    private String child;

    public ActivsRepository(String child) {
        super(new ActivsMapper());
        this.child = child;
        this.databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode());
    }

    public ActivsRepository(String child, String uId) {
        super(new ActivsMapper());
        this.child = child;
        this.databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode()).orderByChild("creatorId").equalTo(uId);
    }

    public ActivsRepository(String rootNode, String child, String attendUserId) {
        super(new ActivsMapper());
        this.child = child;
        this.databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode()).orderByChild(rootNode + "/" +attendUserId).equalTo(true);
    }

    @Override
    protected String getRootNode() {
        return rootNode + "/" + child;
    }

    public void writeNewActiv(Activs activs) {
        String key = ((DatabaseReference) databaseReference).push().getKey();
        activs.activId = key;
        Map<String, Object> postValues = activs.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(key, postValues);

        ((DatabaseReference) databaseReference).updateChildren(childUpdates);
    }
}
