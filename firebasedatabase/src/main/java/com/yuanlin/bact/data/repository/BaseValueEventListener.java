package com.yuanlin.bact.data.repository;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.yuanlin.bact.data.mapper.FirebaseMapper;

import java.util.List;

public class BaseValueEventListener<Model, Entity> implements ValueEventListener {

    private boolean isList;
    private FirebaseMapper<Entity, Model> mapper;
    private Class<Model> modelClass;
    private FirebaseDatabaseRepository.FirebaseDatabaseRepositoryCallback<Model> callback;
    private FirebaseDatabaseRepository.FirebaseDatabaseObjectRepositoryCallback<Model> objectCallback;

    public BaseValueEventListener(FirebaseMapper<Entity, Model> mapper,
                                  FirebaseDatabaseRepository.FirebaseDatabaseRepositoryCallback<Model> callback) {
        this.isList = true;
        this.mapper = mapper;
        this.callback = callback;
    }

    public BaseValueEventListener(Class<Model> modelClass,
                                  FirebaseDatabaseRepository.FirebaseDatabaseObjectRepositoryCallback<Model> callback) {
        this.isList = false;
        this.modelClass = modelClass;
        this.objectCallback = callback;
    }


    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (isList) {
            List<Model> data = mapper.mapList(dataSnapshot);
            callback.onSuccess(data);
        } else {
            objectCallback.onSuccess(dataSnapshot.getValue(modelClass));
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        if (isList) {
            callback.onError(databaseError.toException());
        } else {
            objectCallback.onError(databaseError.toException());
        }
    }
}
