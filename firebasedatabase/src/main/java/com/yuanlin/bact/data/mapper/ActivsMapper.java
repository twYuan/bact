package com.yuanlin.bact.data.mapper;

import com.yuanlin.bact.data.domain.Activs;
import com.yuanlin.bact.data.entity.ActivsEntity;


public class ActivsMapper extends FirebaseMapper<ActivsEntity, Activs> {

    @Override
    public Activs map(ActivsEntity activsEntity) {
        Activs activs = new Activs();
        activs.setCreateDate(activsEntity.getCreateDate());
        activs.setCreatorId(activsEntity.getCreatorId());
        activs.setCreatorName(activsEntity.getCreatorName());
        activs.setDescription(activsEntity.getDescription());
        activs.setLocation(activsEntity.getLocation());
        activs.setEndDate(activsEntity.getEndDate());
        activs.setImg(activsEntity.getImg());
        activs.setMember(activsEntity.getMember());
        activs.setName(activsEntity.getName());
        activs.setStartDate(activsEntity.getStartDate());
        activs.setActivId(activsEntity.getActivId());
        activs.setAttend(activsEntity.getAttend());
        activs.setFavorite(activsEntity.getFavorite());
        return activs;
    }
}
