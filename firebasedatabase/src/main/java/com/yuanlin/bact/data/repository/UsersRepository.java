package com.yuanlin.bact.data.repository;

import com.google.firebase.database.FirebaseDatabase;
import com.yuanlin.bact.data.domain.Users;

public class UsersRepository extends FirebaseDatabaseRepository<Users> {
    private static final String rootNode = "users";
    private String uid;

    public UsersRepository(String uid) {
        super(Users.class);
        this.uid = uid;
        this.databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode());
    }

    @Override
    protected String getRootNode() {
        return rootNode + "/" + uid;
    }
}
