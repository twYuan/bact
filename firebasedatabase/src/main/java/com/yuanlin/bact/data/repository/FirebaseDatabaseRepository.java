package com.yuanlin.bact.data.repository;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.yuanlin.bact.data.mapper.FirebaseMapper;

import java.util.List;

//learn from https://medium.com/globallogic-latinoamerica-mobile/viewmodel-firebase-database-3cc708044b5d

public abstract class FirebaseDatabaseRepository<Model> {

    protected Query databaseReference;
    protected FirebaseDatabaseRepositoryCallback<Model> firebaseCallback;
    protected FirebaseDatabaseObjectRepositoryCallback<Model> firebaseObjectCallback;
    private BaseValueEventListener listener;
    private FirebaseMapper mapper;
    private Class<Model> modelClass;

    protected abstract String getRootNode();

    public FirebaseDatabaseRepository(FirebaseMapper mapper) {
        this.mapper = mapper;
    }

    public FirebaseDatabaseRepository(Class<Model> modelClass) {
        this.modelClass = modelClass;
    }

    public void addListener(FirebaseDatabaseRepositoryCallback<Model> firebaseCallback) {
        this.firebaseCallback = firebaseCallback;
        listener = new BaseValueEventListener(mapper, firebaseCallback);
        databaseReference.addValueEventListener(listener);
    }

    public void addListener(FirebaseDatabaseObjectRepositoryCallback<Model> firebaseCallback) {
        this.firebaseObjectCallback = firebaseCallback;
        listener = new BaseValueEventListener(modelClass, firebaseCallback);
        databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode());
        databaseReference.addValueEventListener(listener);
    }

    public void removeListener() {
        if (databaseReference != null) {
            databaseReference.removeEventListener(listener);
        }
    }

    public interface FirebaseDatabaseRepositoryCallback<T> {
        void onSuccess(List<T> result);

        void onError(Exception e);
    }

    public interface FirebaseDatabaseObjectRepositoryCallback<T> {
        void onSuccess(T result);

        void onError(Exception e);
    }
}