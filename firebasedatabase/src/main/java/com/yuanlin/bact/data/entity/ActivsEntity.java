package com.yuanlin.bact.data.entity;

import java.util.List;
import java.util.Map;

public class ActivsEntity {

    private String createDate;
    private String creatorId;
    private String creatorName;
    private String description;
    private String location;
    private String endDate;
    private List<String> img;
    private List<String> member = null;
    private String name;
    private String startDate;
    private String activId;
    public Map<String,Boolean> attend;
    public Map<String,Boolean> favorite;

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getImg() {
        return img;
    }

    public void setImg(List<String> img) {
        this.img = img;
    }

    public List<String> getMember() {
        return member;
    }

    public void setMember(List<String> member) {
        this.member = member;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getActivId() {
        return activId;
    }

    public void setActivId(String activId) {
        this.activId = activId;
    }

    public Map<String, Boolean> getAttend() {
        return attend;
    }

    public void setAttend(Map<String, Boolean> attend) {
        this.attend = attend;
    }

    public Map<String, Boolean> getFavorite() {
        return favorite;
    }

    public void setFavorite(Map<String, Boolean> favorite) {
        this.favorite = favorite;
    }
}
