package com.yuanlin.bact.data.mapper;

public interface IMapper<From, To> {

    To map(From from);
}