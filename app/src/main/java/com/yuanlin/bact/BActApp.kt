package com.yuanlin.bact

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.yuanlin.bact.common.AppPreferences

class BActApp : Application() {
    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
        AppPreferences.init(this)
    }

}