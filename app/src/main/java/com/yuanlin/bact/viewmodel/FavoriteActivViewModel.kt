package com.yuanlin.bact.viewmodel

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import com.yuanlin.bact.common.AppConfig
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.data.repository.ActivsRepository
import com.yuanlin.bact.data.repository.FirebaseDatabaseRepository


class FavoriteActivViewModel : ViewModel() {

    private var articles: MutableLiveData<List<Activs>>? = null
    private var repository: ActivsRepository? = null
    private var type: String = ""
    private var id: String = ""

    fun getActiv(type: String, id: String): LiveData<List<Activs>>? {
        this.type = type
        this.id = id
        this.repository = ActivsRepository(AppConfig.rootFavorite, type, id)

        if (articles == null) {
            articles = MutableLiveData()
            loadActivs()
        }
        return articles
    }

    override fun onCleared() {
        repository?.removeListener()
    }

    private fun loadActivs() {
        repository?.addListener(object : FirebaseDatabaseRepository.FirebaseDatabaseRepositoryCallback<Activs> {
            override fun onSuccess(result: List<Activs>) {
                articles!!.setValue(result.reversed())
            }

            override fun onError(e: Exception) {
                articles!!.setValue(null)
            }
        })
    }

    fun writeNewPost(activs:Activs) {
        repository?.writeNewActiv(activs)
    }
}
