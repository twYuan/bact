package com.yuanlin.bact.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.data.repository.ActivDetailRepository
import com.yuanlin.bact.data.repository.FirebaseDatabaseRepository

class ActivDetailViewModel : ViewModel() {
    private var type: String = ""
    private var id: String = ""
    private var articles: MutableLiveData<Activs>? = null
    private var repository: ActivDetailRepository? = null

    fun getActivDetail(type: String, id: String): LiveData<Activs>? {
        this.type = type
        this.id = id
        this.repository = ActivDetailRepository(type, id)

        if (articles == null) {
            articles = MutableLiveData()
            loadActiv()
        }
        return articles
    }

    override fun onCleared() {
        repository?.removeListener()
    }

    private fun loadActiv() {
        repository?.addListener(object : FirebaseDatabaseRepository.FirebaseDatabaseObjectRepositoryCallback<Activs> {
            override fun onSuccess(result: Activs?) {
                articles!!.setValue(result)
            }

            override fun onError(e: Exception) {
                articles!!.setValue(null)
            }
        })
    }

    fun writeAttend(attend: Map<String,Boolean>) {
        repository?.writeAttend(attend)
    }

    fun writeFavorite(attend: Map<String,Boolean>) {
        repository?.writeFavorite(attend)
    }
}
