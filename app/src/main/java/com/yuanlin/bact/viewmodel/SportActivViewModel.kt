package com.yuanlin.bact.viewmodel

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.LiveData
import com.yuanlin.bact.common.AppConfig
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.data.repository.ActivsRepository
import com.yuanlin.bact.data.repository.FirebaseDatabaseRepository


class SportActivViewModel : ViewModel() {

    private var articles: MutableLiveData<List<Activs>>? = null
    private val repository = ActivsRepository(AppConfig.childSport)

    fun getActiv(): LiveData<List<Activs>>? {
        if (articles == null) {
            articles = MutableLiveData()
            loadActivs()
        }
        return articles
    }

    override fun onCleared() {
        repository.removeListener()
    }

    private fun loadActivs() {
        repository.addListener(object : FirebaseDatabaseRepository.FirebaseDatabaseRepositoryCallback<Activs> {
            override fun onSuccess(result: List<Activs>) {
                articles!!.setValue(result.reversed())
            }

            override fun onError(e: Exception) {
                articles!!.setValue(null)
            }
        })
    }

    fun writeNewPost(activs:Activs) {
        repository.writeNewActiv(activs)
    }
}
