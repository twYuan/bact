package com.yuanlin.bact.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.data.domain.Users
import com.yuanlin.bact.data.repository.FirebaseDatabaseRepository
import com.yuanlin.bact.data.repository.UsersRepository

class UserViewModel : ViewModel() {

    private var articles: MutableLiveData<Users>? = null
    private val repository = UsersRepository(AppPreferences.uid)

    fun getUser(): LiveData<Users>? {
        if (articles == null) {
            articles = MutableLiveData()
            loadUsers()
        }
        return articles
    }

    override fun onCleared() {
        repository.removeListener()
    }

    private fun loadUsers() {
        repository.addListener(object : FirebaseDatabaseRepository.FirebaseDatabaseObjectRepositoryCallback<Users> {
            override fun onSuccess(result: Users?) {
                articles!!.value = result
            }

            override fun onError(e: Exception) {
                articles!!.setValue(null)
            }
        })
    }
}
