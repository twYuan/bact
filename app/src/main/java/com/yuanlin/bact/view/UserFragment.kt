package com.yuanlin.bact.view

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.yuanlin.bact.R
import com.yuanlin.bact.data.domain.Users
import com.yuanlin.bact.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.user_fragment.*
import kotlinx.coroutines.NonCancellable.cancel

class UserFragment : Fragment() {

    companion object {
        private val TAG: String? = UserFragment::class.simpleName
        fun newInstance() = UserFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.user)
        return inflater.inflate(R.layout.user_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        imageViewUser.setOnClickListener {
            showUserInfoDialog()
        }
        imageViewParticipateActiv.setOnClickListener{
            showAttendActiv()
        }
        imageViewFavorite.setOnClickListener{
            showFavoriteActiv()
        }
        imageViewMyActiv.setOnClickListener{
            showFMyActiv()
        }
    }

    private fun showUserInfoDialog() {
        val newFragment = UserInfoDialogFragment()
        activity?.supportFragmentManager?.let { newFragment.show(it, "missiles") }
    }

    private fun showAttendActiv() {
        val intent = Intent(activity, AttendActiv::class.java)
        startActivity(intent)
    }

    private fun showFavoriteActiv() {
        val intent = Intent(activity, FavoriteActiv::class.java)
        startActivity(intent)
    }

    private fun showFMyActiv() {
        val intent = Intent(activity, MyActiv::class.java)
        startActivity(intent)
    }


}
