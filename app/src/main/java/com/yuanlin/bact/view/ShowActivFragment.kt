package com.yuanlin.bact.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppConfig
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.viewmodel.*

const val ACTIV_TYPE = "com.yuanlin.bact.view.ACTIV_TYPE"
const val ACTIV_ID = "com.yuanlin.bact.view.ACTIV_ID"

class ShowActivFragment : Fragment() {

    companion object {
        private val TAG: String? = ShowActivFragment::class.simpleName
        fun newInstance() = ShowActivFragment()
    }

    private lateinit var viewModel: ViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var showActivAdapter: ShowActivAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.news_fragment, container, false)
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val bundle = this.arguments
        if (bundle != null) {
            val tag = bundle.getString(ACTIV_TYPE, AppConfig.childEat)
            if (tag.equals(AppConfig.childSport)) {
                viewModel = ViewModelProviders.of(this).get(SportActivViewModel::class.java)
                (viewModel as SportActivViewModel).getActiv()?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.sport_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.childAttraction)) {
                viewModel = ViewModelProviders.of(this).get(AttractionActivViewModel::class.java)
                (viewModel as AttractionActivViewModel).getActiv()?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.attraction_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.childEat)) {
                viewModel = ViewModelProviders.of(this).get(EatActivViewModel::class.java)
                (viewModel as EatActivViewModel).getActiv()?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.eat_activ)
                            showActivAdapter = ShowActivAdapter(it)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })

            } else if (tag.equals(AppConfig.rootAttend + "/" + AppConfig.childSport)) {
                viewModel = ViewModelProviders.of(this).get(AttendActivViewModel::class.java)
                (viewModel as AttendActivViewModel).getActiv(
                    AppConfig.childSport,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.joined_activity) + " - " + getString(R.string.sport_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootAttend + "/" + AppConfig.childAttraction)) {
                viewModel = ViewModelProviders.of(this).get(AttendActivViewModel::class.java)
                (viewModel as AttendActivViewModel).getActiv(
                    AppConfig.childAttraction,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.joined_activity) + " - " + getString(R.string.attraction_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootAttend + "/" + AppConfig.childEat)) {
                viewModel = ViewModelProviders.of(this).get(AttendActivViewModel::class.java)
                (viewModel as AttendActivViewModel).getActiv(AppConfig.childEat, AppPreferences.uid)
                    ?.observe(viewLifecycleOwner,
                        Observer<List<Activs>> { activList ->
                            activList?.let {
                                (activity as AppCompatActivity).supportActionBar?.title =
                                    getString(R.string.joined_activity) + " - " + getString(R.string.eat_activ)
                                showActivAdapter = ShowActivAdapter(activList)
                                showActivAdapter.onItemClick = {
                                    val intent =
                                        Intent(context, ActivDetailActivity::class.java).apply {
                                            putExtra(ACTIV_TYPE, tag)
                                            putExtra(ACTIV_ID, it.activId)
                                        }
                                    startActivity(intent)
                                }
                                recyclerView.adapter = showActivAdapter
                            }
                        })
            } else if (tag.equals(AppConfig.rootFavorite + "/" + AppConfig.childSport)) {
                viewModel = ViewModelProviders.of(this).get(FavoriteActivViewModel::class.java)
                (viewModel as FavoriteActivViewModel).getActiv(
                    AppConfig.childSport,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.already_add_to_favorite) + " - " + getString(R.string.sport_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootFavorite + "/" + AppConfig.childAttraction)) {
                viewModel = ViewModelProviders.of(this).get(FavoriteActivViewModel::class.java)
                (viewModel as FavoriteActivViewModel).getActiv(
                    AppConfig.childAttraction,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.already_add_to_favorite) + " - " + getString(R.string.attraction_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootFavorite + "/" + AppConfig.childEat)) {
                viewModel = ViewModelProviders.of(this).get(FavoriteActivViewModel::class.java)
                (viewModel as FavoriteActivViewModel).getActiv(
                    AppConfig.childEat,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.already_add_to_favorite) + " - " + getString(R.string.eat_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootMy + "/" + AppConfig.childSport)) {
                viewModel = ViewModelProviders.of(this).get(MyActivViewModel::class.java)
                (viewModel as MyActivViewModel).getActiv(
                    AppConfig.childSport,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.my_activity) + " - " + getString(R.string.sport_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootMy + "/" + AppConfig.childAttraction)) {
                viewModel = ViewModelProviders.of(this).get(MyActivViewModel::class.java)
                (viewModel as MyActivViewModel).getActiv(
                    AppConfig.childAttraction,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.my_activity) + " - " + getString(R.string.attraction_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            } else if (tag.equals(AppConfig.rootMy + "/" + AppConfig.childEat)) {
                viewModel = ViewModelProviders.of(this).get(MyActivViewModel::class.java)
                (viewModel as MyActivViewModel).getActiv(
                    AppConfig.childEat,
                    AppPreferences.uid
                )?.observe(viewLifecycleOwner,
                    Observer<List<Activs>> { activList ->
                        activList?.let {
                            (activity as AppCompatActivity).supportActionBar?.title =
                                getString(R.string.my_activity) + " - " + getString(R.string.eat_activ)
                            showActivAdapter = ShowActivAdapter(activList)
                            showActivAdapter.onItemClick = {
                                val intent =
                                    Intent(context, ActivDetailActivity::class.java).apply {
                                        putExtra(ACTIV_TYPE, tag)
                                        putExtra(ACTIV_ID, it.activId)
                                    }
                                startActivity(intent)
                            }
                            recyclerView.adapter = showActivAdapter
                        }
                    })
            }

        }
    }

}
