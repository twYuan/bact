package com.yuanlin.bact.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yuanlin.bact.R
import com.yuanlin.bact.data.domain.Activs
import java.text.SimpleDateFormat


class ShowActivAdapter(private val list: List<Activs>) : RecyclerView.Adapter<ShowActivAdapter.ActivsViewHolder>() {

    var onItemClick: ((Activs) -> Unit)? = null

    companion object {
        private val TAG: String? = ShowActivFragment::class.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_list_item, parent, false)
        return ActivsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ActivsViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        Log.d(TAG, "" + list.size)
        return list.size
    }

    inner class ActivsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var title: TextView
        internal var image: ImageView
        internal var time: TextView
        internal var location: TextView

        init {
            Log.d(TAG, "init")
            title = itemView.findViewById(R.id.tv_activ_title)
            image = itemView.findViewById(R.id.iv_news)
            time = itemView.findViewById(R.id.tv_activ_start_time)
            location = itemView.findViewById(R.id.tv_activ_location)
            itemView.setOnClickListener {
                onItemClick?.invoke(list[adapterPosition])
            }
        }

        fun bind(activs: Activs) {
            title.text = activs.name
            val sdf = SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss")
            time.text = sdf.format(activs.startDate.toLong() * 1000)
            location.text = activs.location

            Glide.with(image.context)
                .load(activs.img[0])
                .into(image)
        }
    }
}