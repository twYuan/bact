package com.yuanlin.bact.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppConfig

class AttendActiv : AppCompatActivity() {

    private val showEatActivFragment: Fragment = ShowActivFragment()
    private val showSportActivFragment: Fragment = ShowActivFragment()
    private val showAttractionActivFragment: Fragment = ShowActivFragment()

    private val fm: FragmentManager = supportFragmentManager
    private var active = showEatActivFragment

    companion object {
        val TAG = "MainActivity"
    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_eat -> {
                fm.beginTransaction().hide(active).show(showEatActivFragment).commit()
                active = showEatActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sport -> {
                fm.beginTransaction().hide(active).show(showSportActivFragment).commit()
                active = showSportActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_attraction -> {
                fm.beginTransaction().hide(active).show(showAttractionActivFragment).commit()
                active = showAttractionActivFragment
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.attend_activity)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val showEatActivBundle = Bundle()
        showEatActivBundle.putString(ACTIV_TYPE, AppConfig.rootAttend + "/" + AppConfig.childEat)
        showEatActivFragment.arguments = showEatActivBundle

        val showSportActivBundle = Bundle()
        showSportActivBundle.putString(ACTIV_TYPE, AppConfig.rootAttend + "/" + AppConfig.childSport)
        showSportActivFragment.arguments = showSportActivBundle

        val showAttractionActivBundle = Bundle()
        showAttractionActivBundle.putString(ACTIV_TYPE, AppConfig.rootAttend + "/" + AppConfig.childAttraction)
        showAttractionActivFragment.arguments = showAttractionActivBundle

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        if(fm.beginTransaction().isEmpty){
            fm.beginTransaction().add(R.id.main_container, showEatActivFragment, "1")
                .add(R.id.main_container, showSportActivFragment, "2")
                .add(R.id.main_container, showAttractionActivFragment, "3")
                .commit()
        }
        fm.beginTransaction().hide(showEatActivFragment)
            .hide(showSportActivFragment)
            .hide(showAttractionActivFragment)
            .show(showEatActivFragment).commit()
    }
}
