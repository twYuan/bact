package com.yuanlin.bact.view

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.common.DownloadImageTask
import com.yuanlin.bact.common.Utility
import kotlinx.android.synthetic.main.dialog_show_user_info.view.*
import java.io.InputStream
import java.net.URL


class UserInfoDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;
            val view: View = inflater.inflate(R.layout.dialog_show_user_info, null)
            view.textViewEmail.setText(AppPreferences.email)
            view.textViewName.setText(AppPreferences.userInfo.username)
            DownloadImageTask(view.imageViewUser).execute(AppPreferences.userInfo.profileImageUrl)
            builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.finish,
                    DialogInterface.OnClickListener { dialog, id ->
                        getDialog()?.cancel()
                    })
            builder.create()


        } ?: throw IllegalStateException("Activity cannot be null")
    }
}