package com.yuanlin.bact.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ServerValue
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppConfig
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.data.domain.Users
import com.yuanlin.bact.viewmodel.UserViewModel


class MainActivity : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private lateinit var viewModel: UserViewModel

    private val showEatActivFragment: Fragment = ShowActivFragment()
    private val showSportActivFragment: Fragment = ShowActivFragment()
    private val showAttractionActivFragment: Fragment = ShowActivFragment()
    private val newActivFragment: Fragment = NewActivFragment()
    private val userFragment: Fragment = UserFragment()
    private val fm: FragmentManager = supportFragmentManager
    private var active = showEatActivFragment

    companion object {
        val TAG = "MainActivity"
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_eat -> {
                fm.beginTransaction().hide(active).show(showEatActivFragment).commit()
                active = showEatActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sport -> {
                fm.beginTransaction().hide(active).show(showSportActivFragment).commit()
                active = showSportActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_attraction -> {
                fm.beginTransaction().hide(active).show(showAttractionActivFragment).commit()
                active = showAttractionActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_add -> {
                supportActionBar?.title = getString(R.string.add_activity)
                fm.beginTransaction().hide(active).show(newActivFragment).commit()
                active = newActivFragment
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_user -> {
                supportActionBar?.title = getString(R.string.user)
                fm.beginTransaction().hide(active).show(userFragment).commit()
                active = userFragment
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        verifyUserIsLoggedIn()
        database = FirebaseDatabase.getInstance().reference
        if (!AppPreferences.firstRun) {
            AppPreferences.firstRun = true
            Log.d("TAG", "The value of our pref is: ${AppPreferences.firstRun}")
        }

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val showEatActivBundle = Bundle()
        showEatActivBundle.putString(ACTIV_TYPE, AppConfig.childEat)
        showEatActivFragment.setArguments(showEatActivBundle)

        val showSportActivBundle = Bundle()
        showSportActivBundle.putString(ACTIV_TYPE, AppConfig.childSport)
        showSportActivFragment.setArguments(showSportActivBundle)

        val showAttractionActivBundle = Bundle()
        showAttractionActivBundle.putString(ACTIV_TYPE, AppConfig.childAttraction)
        showAttractionActivFragment.setArguments(showAttractionActivBundle)

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        if(fm.beginTransaction().isEmpty){
            fm.beginTransaction().add(R.id.main_container, showEatActivFragment, "1")
                .add(R.id.main_container, showSportActivFragment, "2")
                .add(R.id.main_container, showAttractionActivFragment, "3")
                .add(R.id.main_container, newActivFragment, "4")
                .add(R.id.main_container, userFragment, "5")
                .commit()
        }
        fm.beginTransaction().hide(showEatActivFragment)
            .hide(showSportActivFragment)
            .hide(showAttractionActivFragment)
            .hide(newActivFragment)
            .hide(userFragment)
            .show(showEatActivFragment).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_sign_out -> {
                signOut();
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun verifyUserIsLoggedIn() {
        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            viewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
            (viewModel as UserViewModel).getUser()?.observe(this,
                Observer<Users> { user ->
                    if(user == null) {
                        signOut()
                    } else {
                        //save userInfo to SharedPreference
                        AppPreferences.userInfo = user
                    }
                }
            )
        }
    }

    private fun signOut() {
        val uid = FirebaseAuth.getInstance().uid
        uid?.let {
            var userMap = mapOf("tokenId" to "", "lastOnline" to ServerValue.TIMESTAMP)

            database.child("/users/$uid")
                .updateChildren(userMap)
                .addOnSuccessListener {
                    FirebaseAuth.getInstance().signOut()
                    val intent = Intent(this, RegisterActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }.addOnFailureListener {
                    Log.e(TAG, "update token fail");
                }
        }
    }
}
