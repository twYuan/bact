package com.yuanlin.bact.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppConfig
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.viewmodel.ActivDetailViewModel
import kotlinx.android.synthetic.main.activ_detail_activity.*
import java.text.SimpleDateFormat

class ActivDetailActivity : AppCompatActivity() {
    private lateinit var type: String
    private lateinit var id: String
    private lateinit var activDetailViewModel: ActivDetailViewModel
    private lateinit var activs: Activs

    companion object {
        val TAG = "ActivDetailActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activ_detail_activity)

        type = intent.getStringExtra(ACTIV_TYPE)
        type = type.replace(AppConfig.rootAttend + "/", "")
        type = type.replace(AppConfig.rootFavorite + "/", "")
        type = type.replace(AppConfig.rootMy + "/", "")
        id = intent.getStringExtra(ACTIV_ID)

        activDetailViewModel = ViewModelProviders.of(this).get(ActivDetailViewModel::class.java)
        activDetailViewModel.getActivDetail(type, id)?.observe(this,
            Observer<Activs> {
                activs = it
                textViewName.text = it.name
                textViewCreateDate.text = it.startDate
                textViewDescription.text = it.description
                if (it.attend.containsKey(AppPreferences.uid)) buttonParticipate.text = getString(R.string.joined_activity)
                else buttonParticipate.text = getString(R.string.join_activity)
                if (it.favorite.containsKey(AppPreferences.uid)) buttonFavorite.text = getString(R.string.already_add_to_favorite)
                else buttonFavorite.text = getString(R.string.add_favorite)
                val sdf = SimpleDateFormat("yyyy/MM/dd/   HH:mm:ss")
                textViewCreateDate.text = sdf.format(it.startDate.toLong() * 1000)
                Glide.with(this)
                    .load(it.img[0])
                    .placeholder(R.drawable.banana)
                    .into(imageViewActivImg)
            })

        buttonParticipate.setOnClickListener {
            if (buttonParticipate.text.equals(getString(R.string.join_activity))) addToAttend()
            else this.removeFromAttend()
        }

        buttonFavorite.setOnClickListener {
            if (buttonFavorite.text.equals(getString(R.string.add_favorite))) addToFavorite()
            else this.removeFromFavorite()
        }
    }

    private fun addToAttend() {
        activs.attend[AppPreferences.uid] = true
        activDetailViewModel.writeAttend(activs.attend)
    }

    private fun removeFromAttend() {
        activs.attend.remove(AppPreferences.uid)
        activDetailViewModel.writeAttend(activs.attend)
    }

    private fun addToFavorite() {
        activs.attend[AppPreferences.uid] = true
        activDetailViewModel.writeFavorite(activs.attend)
    }

    private fun removeFromFavorite() {
        activs.attend.remove(AppPreferences.uid)
        activDetailViewModel.writeFavorite(activs.attend)
    }
}
