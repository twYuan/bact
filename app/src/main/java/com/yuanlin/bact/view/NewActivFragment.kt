package com.yuanlin.bact.view

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.storage.FirebaseStorage
import com.yuanlin.bact.R
import com.yuanlin.bact.common.AppPreferences
import com.yuanlin.bact.data.domain.Activs
import com.yuanlin.bact.viewmodel.AttractionActivViewModel
import com.yuanlin.bact.viewmodel.EatActivViewModel
import com.yuanlin.bact.viewmodel.SportActivViewModel
import kotlinx.android.synthetic.main.new_fragment.*
import java.text.SimpleDateFormat
import java.util.*


class NewActivFragment : Fragment() {

    companion object {
        fun newInstance() = NewActivFragment()
        val TAG = "NewActivFragment"
    }

    private lateinit var viewModel: ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.add_activity)
        return inflater.inflate(R.layout.new_fragment, container, false)
    }

    private var ts: String? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        bt_submit.setOnClickListener {

            val dateFormat = SimpleDateFormat("yyyy/M/d H:m")
            val parsedDate = dateFormat.parse(edt_date.text.toString() + " " + edt_time.text.toString())
            ts = (parsedDate.time / 1000).toString()
            uploadImageToFirebaseStorage()
            progressBar.visibility = View.VISIBLE
        }

        imageButton1.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        imageButton2.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 1)
        }

        imageButton3.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)
        }

        imageButton4.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 3)
        }

        imageButton5.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 4)
        }

        val sdfDate = SimpleDateFormat("yyyy/M/d")
        val currentDate = sdfDate.format(Date())
        edt_date.setText(currentDate)
        edt_date.inputType = InputType.TYPE_NULL;
        edt_date.setOnClickListener(View.OnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                activity,
                OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    edt_date.setText("" + year + "/" + (monthOfYear + 1) + "/" + dayOfMonth)
                },
                year,
                month,
                day
            )
            dpd.show()
        })

        val sdfTime = SimpleDateFormat("H:m")
        val currentDateTime = sdfTime.format(Date())
        edt_time.setText(currentDateTime)
        edt_time.inputType = InputType.TYPE_NULL;
        edt_time.setOnClickListener(View.OnClickListener {
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minute = c.get(Calendar.MINUTE)
            val tpd = TimePickerDialog(activity, TimePickerDialog.OnTimeSetListener { _, h, m ->
                edt_time.setText(h.toString() + ":" + m.toShort())
            }, hour, minute, false)
            tpd.show()
        })
    }

    fun onSubmitButtonClicked() {
        for (i in 0 .. selectedPhotoUri.size-1) {
            if(selectedPhotoUri[i] != null && imgArray[i] == null)
                return
        }

        val memberList = emptyList<String>()
        var activs = Activs(
            ts,
            AppPreferences.uid,
            et_name.text.toString(),
            et_description.text.toString(),
            et_location.text.toString(),
            ts,
            imgArray.toList(),
            memberList,
            et_name.text.toString(),
            ts
        )

        if (radioButtonEat.isChecked) {
            viewModel = ViewModelProviders.of(this).get(EatActivViewModel::class.java)
            (viewModel as EatActivViewModel).writeNewPost(activs)
        }
        if (radioButtonSport.isChecked) {
            viewModel = ViewModelProviders.of(this).get(SportActivViewModel::class.java)
            (viewModel as SportActivViewModel).writeNewPost(activs)
        }
        if (radioButtonAttraction.isChecked) {
            viewModel = ViewModelProviders.of(this).get(AttractionActivViewModel::class.java)
            (viewModel as AttractionActivViewModel).writeNewPost(activs)
        }
        progressBar.visibility = View.GONE
    }

    var selectedPhotoUri: Array<Uri?> = arrayOfNulls<Uri>(5)
    var imgArray: Array<String?> = arrayOfNulls<String>(5)

    var selectedPhotoUri1: Uri? = null
    var selectedPhotoUri2: Uri? = null
    var selectedPhotoUri3: Uri? = null
    var selectedPhotoUri4: Uri? = null
    var selectedPhotoUri5: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri1 = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri1)
            imageButton1.setImageBitmap(bitmap)
        }else if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri2 = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri2)
            imageButton2.setImageBitmap(bitmap)
        }else if (requestCode == 2 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri3 = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri3)
            imageButton3.setImageBitmap(bitmap)
        }else if (requestCode == 3 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri4 = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri4)
            imageButton4.setImageBitmap(bitmap)
        }else if (requestCode == 4 && resultCode == Activity.RESULT_OK && data != null) {
            selectedPhotoUri5 = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, selectedPhotoUri5)
            imageButton5.setImageBitmap(bitmap)
        }
    }

    private fun uploadImageToFirebaseStorage() {
        if (selectedPhotoUri1 == null && selectedPhotoUri2 == null && selectedPhotoUri3 == null && selectedPhotoUri4 == null && selectedPhotoUri5 ==null) return

        selectedPhotoUri[0] = selectedPhotoUri1
        selectedPhotoUri[1] = selectedPhotoUri2
        selectedPhotoUri[2] = selectedPhotoUri3
        selectedPhotoUri[3] = selectedPhotoUri4
        selectedPhotoUri[4] = selectedPhotoUri5

        for (i in 0 .. selectedPhotoUri.size -1) {
            if(selectedPhotoUri[i] != null) {
                val fileName =  AppPreferences.uid + "-" + ts + "-image" + i
                val ref = FirebaseStorage.getInstance().getReference("/activ/$fileName")
                ref.putFile(selectedPhotoUri[i]!!)
                    .addOnSuccessListener {
                        Log.d(TAG, "Successfully uploaded image: ${it.metadata?.path}")

                        ref.downloadUrl.addOnSuccessListener {
                            Log.d(TAG, "File Location: $it")
                            if(ref.name.contains("image0")) {
                                imgArray[0] = it.toString()
                            }
                            if(ref.name.contains("image1")) {
                                imgArray[1] = it.toString()
                            }
                            if(ref.name.contains("image2")) {
                                imgArray[2] = it.toString()
                            }
                            if(ref.name.contains("image3")) {
                                imgArray[3] = it.toString()
                            }
                            if(ref.name.contains("image4")) {
                                imgArray[4] = it.toString()
                            }

                            onSubmitButtonClicked()
                        }
                    }
                    .addOnFailureListener {
                        progressBar.visibility = View.GONE
                        Log.d(TAG, "Failed to upload image to storage: ${it.message}")
                    }
            }
        }

    }
}
