package com.yuanlin.bact.common

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.yuanlin.bact.data.domain.Users


object AppPreferences {
    private const val NAME = "bActiv"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    private val IS_FIRST_RUN_PREF = Pair("is_first_run", false)
    private val USER_UID = Pair("user_uid", "")
    private val USER_EMAIL = Pair("user_email", "")
    private val USER_INFO = Pair("user_info", null)

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var firstRun: Boolean
        // custom getter to get a preference of a desired type, with a predefined default value
        get() = preferences.getBoolean(IS_FIRST_RUN_PREF.first, IS_FIRST_RUN_PREF.second)

        // custom setter to save a preference back to preferences file
        set(value) = preferences.edit {
            it.putBoolean(IS_FIRST_RUN_PREF.first, value)
        }

    var uid: String
        // custom getter to get a preference of a desired type, with a predefined default value
        get() = preferences.getString(USER_UID.first, USER_UID.second)

        // custom setter to save a preference back to preferences file
        set(value) = preferences.edit {
            it.putString(USER_UID.first, value)
        }

    var email: String
        // custom getter to get a preference of a desired type, with a predefined default value
        get() = preferences.getString(USER_EMAIL.first, USER_EMAIL.second)

        // custom setter to save a preference back to preferences file
        set(value) = preferences.edit {
            it.putString(USER_EMAIL.first, value)
        }

    var userInfo: Users
        // custom getter to get a preference of a desired type, with a predefined default value
        get() = Gson().fromJson(preferences.getString(USER_INFO.first, USER_UID.second), Users::class.java)

        // custom setter to save a preference back to preferences file
        set(value) = preferences.edit {
            val gson = Gson();
            val json= gson.toJson(value);
            it.putString(USER_INFO.first, json)
        }
}