package com.yuanlin.bact.common

class AppConfig {
    companion object {
        const val rootActiv = "Activity"
        const val rootUser = "users"
        const val rootFavoriteActiv = "FavActivity"
        const val rootParticipateActiv = "ParticipateActivity"
        const val childEat = "eat"
        const val childSport = "sport"
        const val childAttraction = "attraction"
        const val rootAttend = "attend"
        const val rootFavorite = "favorite"
        const val rootMy = "my"
    }

}